<?php return array (
  'aloha/twilio' => 
  array (
    'providers' => 
    array (
      0 => 'Aloha\\Twilio\\Support\\Laravel\\ServiceProvider',
    ),
    'aliases' => 
    array (
      'Twilio' => 'Aloha\\Twilio\\Support\\Laravel\\Facade',
    ),
  ),
  'dirape/token' => 
  array (
    'providers' => 
    array (
      0 => 'Dirape\\Token\\TokenServiceProvider',
    ),
    'aliases' => 
    array (
      'Token' => 'Dirape\\Token\\Facades\\Facade',
    ),
  ),
  'fideloper/proxy' => 
  array (
    'providers' => 
    array (
      0 => 'Fideloper\\Proxy\\TrustedProxyServiceProvider',
    ),
  ),
  'laravel/tinker' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Tinker\\TinkerServiceProvider',
    ),
  ),
  'laravelcollective/html' => 
  array (
    'providers' => 
    array (
      0 => 'Collective\\Html\\HtmlServiceProvider',
    ),
    'aliases' => 
    array (
      'Form' => 'Collective\\Html\\FormFacade',
      'Html' => 'Collective\\Html\\HtmlFacade',
    ),
  ),
  'nunomaduro/collision' => 
  array (
    'providers' => 
    array (
      0 => 'NunoMaduro\\Collision\\Adapters\\Laravel\\CollisionServiceProvider',
    ),
  ),
);