<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['namespace' => 'api'], function(){
	Route::post('login', 'AuthController@login');
    Route::post('register','AuthController@register');
    Route::post('verify','AuthController@verify');
    Route::post('forgot-password','AuthController@forgotPassword');
    Route::post('reset-password','AuthController@resetPassword');
    Route::post('resend-link','AuthController@resendLink');
    //Logout user
	Route::post('logout','AuthController@logout');
	Route::group(['middleware' => ['ApiAuth','TestSession']], function()
	{
	   Route::post('edit-profile','UserController@editProfile');
	   Route::post('update-profile','UserController@updateProfile');
	   Route::post('view-profile','UserController@viewProfile');
	});
});




