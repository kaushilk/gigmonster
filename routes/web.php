<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
    /*use App\model\ApiSession;

    Route::get('/testdata',function(){
      $data=ApiSession::GetUser('236b9036fbf794541bef8e4b84483cf4');
      dd($data);
    });*/

Route::get('/', function ()
{
    if (Auth::check()) {
      return redirect()->intended('dashboard');
    }
    return view('admin.newlogin');
});



Route::group(['prefix' => 'admin','namespace' => 'admin'], function()
   {
      Route::post('login','LoginController@login');
      Route::post('register','LoginController@register');
      Route::get('verify/{code}','LoginController@Verify');
      Route::group(['middleware' => ['AuthUser','TestSession']], function()
      {
            Route::get('logout','LoginController@logout');
            Route::get('dashboard','HomeController@dashboard');

            Route::group(['prefix' =>'category'], function(){
                 Route::resource('/','CategoryController') ;
            });

      });
});

    // Route::group(['prefix' => 'admin','namespace' => 'admin','middleware' => 'AuthUser'], function() {
    //     Route::get('/','HomeController@dashboard');
    //     Route::get('dashboard','HomeController@dashboard');
    // });    

    
    


