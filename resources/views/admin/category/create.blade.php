@extends('layouts.admin')
@section('title')
Add Tournament | Sporta-loca
@endsection
@section('pagelevel_css')

@endsection
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">

            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Add Category
                        </h3>
                    </div>
                </div>
            </div>


            <div class="m-portlet__body">
                <div class="row">
                    <div class="col-md-12">
                        @if(Session::has('message'))
                            {!! Session::get('message') !!}
                        @endif
                    </div>
                </div>

                    @if ($errors->any())
                        <div class="alert alert-danger col-md-5" role="alert">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif    

                <div class="col-lg-20">
								<!--begin::Portlet-->
					<div class="m-portlet">

                        {{  Form::open(array('url'=>'admin/category' , 'method' =>'POST','class'=>'m-form','files'=>'true'))}}
                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">
                                    <div class="form-group m-form__group">
                                        <label for="example_input_full_name">
                                            Tournament Type:
                                        </label>
                                        <!-- <input type="text" class="form-control m-input" placeholder="Enter Country Name"> -->
                                        {{ Form::select('tournament_type',['round_robin'=>'round_robin','double_round_robin'=>'double_round_robin','cup'=>'cup'],'',array('class'=>'form-control m-input tournament-type','data-required'=>'1','placeholder'=>'Select Tournament Type'))}}
                                    </div>
                                </div>
                            </div>

                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">
                                    <div class="form-group m-form__group">
                                        <label for="example_input_full_name">
                                            Tournament Name:
                                        </label>
                                        {{ Form::text('tournament_name','',array('class'=>'form-control m-input','data-required'=>'1'))}}
                                    </div>
                                </div>
                            </div>

                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">
                                    <div class="form-group m-form__group">
                                        <label for="example_input_full_name">
                                            Tournament Date:
                                        </label>
                                        {{ Form::text('date','',array('class'=>'form-control m-input','data-required'=>'1','id' => 'm_daterangepicker_1'))}}
                                    </div>
                                </div>
                            </div>
                           
                            <div class="m-portlet__body">
                                <div class="form-group m-form__group">
                                        <label for="example_input_full_name">
                                           Select City:
                                        </label>
                                        {{ Form::select('city',[],null,array('class'=>'form-control m-select2 city','data-required'=>'1','id'=>'m_select2_11'))}}
                                </div>
                            </div>


                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">
                                    <div class="form-group m-form__group">
                                        <label for="example_input_full_name">
                                            Gender:
                                        </label>
                                        {{ Form::select('gender',['Men'=>'Men','Women'=>'Women','Mixed'=>'Mixed'],'',array('class'=>'form-control m-input tournament-type','data-required'=>'1','placeholder'=>'Select Gender'))}}
                                    </div>
                                </div>
                            </div>


                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">
                                    <div class="form-group m-form__group">
                                        <label for="example_input_full_name">
                                         Age:
                                        </label>
                                        {{ Form::text('age','',array('class'=>'form-control m-input','data-required'=>'1'))}}
                                    </div>
                                </div>
                            </div>

                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">
                                    <div class="form-group m-form__group">
                                        <label for="example_input_full_name">
                                         Amount :
                                        </label>
                                        {{ Form::number('fees','',array('class'=>'form-control m-input','data-required'=>'1'))}}
                                    </div>
                                </div>
                            </div>

                            
                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">
                                    <div class="form-group m-form__group">
                                        <label for="example_input_full_name">
                                         prize_description :
                                        </label>
                                        {{ Form::text('prize_description','',array('class'=>'form-control m-input','data-required'=>'1'))}}
                                    </div>
                                </div>
                            </div>

                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">
                                    <div class="form-group m-form__group">
                                        <label for="example_input_full_name">
                                        Tournament Logo:
                                        </label>
                                        {{ Form::file('logo',array('class'=>'form-control m-input','data-required'=>'1'))}}
                                    </div>
                                </div>
                            </div>

                            <div class="m-portlet__body">
                                <div class="form-group m-form__group">
                                    <label for="example_input_full_name">
                                    Tournament Description:
                                    </label>
                                </div>    
                                {{Form::textarea('description','',array('class'=>'form-control m-input','rows'=>'3'))}}
                            </div>

                            <div class="m-portlet__body">
                                <div class="form-group m-form__group">
                                    <label for="example_input_full_name">
                                    Organize Detail:
                                    </label>
                                </div>    
                                {{Form::textarea('organizer_detail','',array('class'=>'form-control m-input','rows'=>'3'))}}
                            </div>

                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">
                                    <div class="form-group m-form__group">
                                        <label for="example_input_full_name">
                                           Select Tournament Status:
                                        </label>
                                        {{ Form::select('status',['Active'=>'Active','Deactive'=>'Deactive'],'',array('class'=>'form-control m-input','data-required'=>'1','placeholder'=>'Select Tournament Status'))}}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                               <div class="col-md-4">
                                   {!! Form::hidden('location_latitude',old('lat'), ['id'=>'lat']) !!}
                                   {!! Form::hidden('location_longitude',old('lng'), ['id'=>'lng']) !!}
                               </div>
                           </div>

                            <!-- <div class="m-portlet__body">
                               <div class="m-form__section m-form__section--first">
                                    <div class="form-group m-form__group">
                                        <label for="example_input_full_name">
                                            Choose Map Location:
                                        </label>
                                    </div>
                                </div>    
                            </div>    

                                <input id="pac-input" class="controls" type="text" placeholder="Search Box" name="location" style="height: 30px; width: 500px;">
                                <div class="col-md-5 col-md-offset-3" style="height: 400px; margin-bottom: 10px" id="map"></div> -->

                            <div class="m-portlet__foot m-portlet__foot--fit">
                                <div class="m-form__actions m-form__actions">
                                    {{ Form::submit('submit',array('class'=>'btn btn-primary')) }}
                                    <a href="{{ URL::to('admin/tournament')}}" class='btn btn-secondary'>Cancel</a>
                                </div>
                            </div>

                            <!-- <div id="locationField">
                                <input id="autocomplete" placeholder="Enter your address"
                                        onFocus="geolocate()" type="text"></input>
                            </div> -->

                        <!-- <div style="display:none">
                            <table id="address">
                                <tr>
                                    <td class="label">Street address</td>
                                    <td class="slimField"><input class="field" id="street_number"
                                        disabled="true"></input></td>
                                    <td class="wideField" colspan="2"><input class="field" id="route"
                                        disabled="true"></input></td>
                                </tr>
                                <tr>
                                    <td class="label">City</td>
                                    <td class="wideField" colspan="3"><input class="field" id="locality" name="city"
                                        disabled="true"></input></td>
                                </tr>
                                <tr>
                                    <td class="label">State</td>
                                    <td class="slimField"><input class="field"
                                        id="administrative_area_level_1" name="state" disabled="true"></input></td>
                                    <td class="label">Zip code</td>
                                    <td class="wideField"><input class="field" id="postal_code"
                                        disabled="true"></input></td>
                                </tr>
                                <tr>
                                    <td class="label">Country</td>
                                    <td class="wideField" colspan="3"><input class="field"
                                        id="country" disabled="true"></input></td>
                                </tr>
                            </table>
                        </div>     -->


                       {{Form::close()}}
                    </div>
                </div>    
            </div>
        </div>
    </div>
</div>
@endsection
@section('pagelevel_script')

@endsection

