<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<title>Gig Monster Email</title>
<style type="text/css">
body, table, td, p, a, li, blockquote {
  -webkit-text-size-adjust: none !important;
  margin: 0px;
  font-family: "arial", sans-serif !important;
}
body, table, td {
  font-family: "arial", sans-serif !important;
}
table.upper {
  border-right: 1px solid #eaeff2;
  border-left: 1px solid #eaeff2;
  background: #fff;
}
table.second, table.third {
  border-bottom: 1px solid #eaeff2;
}
tr.border_bottom {
  border-bottom: 1px solid #eaeff2;
}

.header tr td a{display: inline-block;
width: 100%;
text-align: center;}

.header tr td a img{width: 500px;
margin: 0px auto;}

@media only screen and (max-width: 750px) {
table {
  width: 100% !important;
  font-family: "arial", sans-serif;
}
table.first {
  padding: 0px 10px;
  width: 100% !important;
}
table.second tr td p {
  margin: 0px 5px !important;
  font-size: 12px !important;
  line-height: 20px !important;
}
.andriod {
  padding: 0px 10px;
}
.image-responsive {
  width: 100% !important;
}
table.first tr td {
  padding: 0px 0px !important;
}
table.third tr td {
  padding: 0px 10px !important;
}
.thirdb img, .first-image td img {
  width: 100%;
}
.space-margin img {
  display: none;
}
}
</style>
</head>
<body style="margin:0px; min-width:320px;">
<table cellpadding="0" cellspacing="0" width="700" align="center" style="margin:0px auto; background: #fff;">
  <tr style="height:40px;">
    <td></td>
  </tr>
</table>
<table cellpadding="0" cellspacing="0" width="700" align="center" style="margin:0px auto; background: #fff;">
  <tr>
    <td><table cellpadding="0" cellspacing="0" width="700" align="center" style="background:transparent; border:1px solid #ccc; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; margin:0px auto;">
        <tr>
          <td><table class="header" cellpadding="0" cellspacing="0" width="700" align="center" style="margin:0px auto;">
              <tr>
                <td><a href="http://gigmonster.com">
                <img class="image-responsive" src="http://gigmonster.com/app/assets/images/new-logo.png" alt="image description"></a></td>
              </tr>
            </table>
            <table class="combine-two" cellpadding="0" cellspacing="0" width="700" align="center" style="margin:0px auto;">
              <tr>
                <td>
                
                  <table class="first" cellpadding="0" cellspacing="0" width="590" align="center" style= "margin:0px auto; text-align: center;">
                    <tr style="height:40px;">
                      <td></td>
                    </tr>
                    <tr>
                      <td align="left" style="color:#000000; font-size:28px; font-weight:700;">Hello {{$user['name']}}</td>
                    </tr>
                    <tr style="height:40px;">
                      <td></td>
                    </tr>
                    <tr>
                      <td align="left" style="color:#000000; font-size:18px; line-height:25px;">Thanks for signing up with Gigmonster.com. To use your account, you will first need to verify your account. Please use below code in the verification form on gigmonster</td>
                    </tr>
                    <tr style="height:40px;">
                      <td></td>
                    </tr>
                    <tr>
                      
                      <td align="center" width="200" style="color:#b40500; font-size:20px; font-style:italic; font-weight:700; text-align:center; border:1px solid #000; padding:5px;">{{$user['code']}}</td>
                    </tr>
                    
                    <tr style="height:40px;">
                      <td></td>
                    </tr>
                  
                   
                  </table>
                  
                </td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>