<!-- BEGIN: Aside Menu -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
				<!-- BEGIN: Left Aside -->
				<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
					<i class="la la-close"></i>
				</button>
				<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="0" m-menu-dropdown-timeout="500" >
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
          
           <li class="m-menu__item  m-menu__item--submenu {{ Request::is('admin/dashboard*') ? 'm-menu__item--active' : ''}}">
                <a  href="{{url::to('admin/dashboard')}}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-line-graph"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Dashboard
                            </span>
                        </span>
                    </span>
                </a>
            </li>
            <li class="m-menu__item  m-menu__item--submenu {{ Request::is('admin/category*') ? 'm-menu__item--active' : ''}}">
                <a  href="{{url::to('admin/category')}}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-line-graph"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Category
                            </span>
                        </span>
                    </span>
                </a>
            </li>
            
           <!--  <li class="m-menu__item  m-menu__item--submenu {{ Request::is('admin/team*') ? 'm-menu__item--active' : ''}}">
            <a  href="{{ url('admin/team') }}" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon fa fa-cog"></i>
                <span class="m-menu__link-text">
                    Teams
                </span>
            </a>
        </li> -->
         
        </ul>
    </div>
</div>