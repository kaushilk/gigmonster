<?php

use Illuminate\Database\Seeder;
use App\model\Categories;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category=\DB::connection('mysql_external')->table('category')->get();
        foreach ($category as $value) {
        	$category_data[]=[
        		'name'=>$value->name,'color'=>$value->color,'created_at'=>Carbon\Carbon::now(),'updated_at'=>Carbon\Carbon::now()
        	];
        }
        Categories::insert($category_data);
    }
}
