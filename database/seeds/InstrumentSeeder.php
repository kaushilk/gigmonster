<?php

use Illuminate\Database\Seeder;
use App\model\Instruments;

class InstrumentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $instrument_list=\DB::connection('mysql_external')->table('instruments_list')->get();
        foreach ($instrument_list as $value) {
        	$instrument[]=['name'=>$value->instruments_name,'sub_instrument_id'=>$value->parent_id,'created_at'=>Carbon\Carbon::now(),'updated_at'=>Carbon\Carbon::now()];
        }
		Instruments::insert($instrument);
    }
}
