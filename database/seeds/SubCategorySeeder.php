<?php

use Illuminate\Database\Seeder;
use App\model\SubCategories;

class SubCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sub_category=\DB::connection('mysql_external')->table('sub_categories')->get();
        foreach ($sub_category as $value) {
        	$subcategory_data[]=[
        		'subcategory_name'=>$value->subcat_name,'category_id'=>$value->cat_id,'created_at'=>Carbon\Carbon::now(),'updated_at'=>Carbon\Carbon::now()
        	];
        }
        SubCategories::insert($subcategory_data);
    }
}
