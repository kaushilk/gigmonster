<?php

use Illuminate\Database\Seeder;
use App\model\UserGeners;

class UserGenersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_geners=\DB::connection('mysql_external')->table('users_genres')->get();
        foreach ($user_geners as $value) {
        	$generes_data[]=['genres_names'=>$value->genres_names,'created_at'=>Carbon\Carbon::now(),'updated_at'=>Carbon\Carbon::now()];
        }
        UserGeners::insert($generes_data);
    }
}
