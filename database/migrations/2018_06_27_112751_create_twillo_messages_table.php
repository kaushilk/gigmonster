<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTwilloMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('twillo_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('sender_id');
            $table->string('sender_number', 30);
            $table->string('twilio_phone', 30);
            $table->string('sender_message_id', 60);
            $table->text('user_message');    
            $table->unsignedInteger('receiver_id');
            $table->string('receiver_number', 30);
            $table->string('type', 30);
            $table->enum('message_status', ['read','unread']);
            $table->string('direction', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('twillo_messages');
    }
}
