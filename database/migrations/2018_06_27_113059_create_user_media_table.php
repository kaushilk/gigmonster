<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_media', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->enum('type', ['music','video','image']);
            $table->string('music_title')->nullable();
            $table->char('music_year',4)->nullable();
            $table->string('music_album')->nullable();
            $table->string('music_genre')->nullable();
            $table->string('tracknumber')->nullable();
            $table->text('music_comments')->nullable();
            $table->string('music_file',40)->nullable();
            $table->string('video_tilte',100)->nullable();
            $table->string('video_url',100)->nullable();
            $table->string('image_title',100)->nullable();
            $table->text('image_description')->nullable();
            $table->string('image_file',40)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_media');
    }
}
