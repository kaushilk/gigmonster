<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('social_id')->nullable();
            $table->string('member_type',100)->nullable();
            $table->string('first_name',50);
            $table->string('last_name',50);
            $table->string('country',70);
            $table->string('state',70);
            $table->string('city',70);
            $table->char('zipcode',6);
            $table->string('phone',40);
            $table->string('twilio_sid',100)->nullable();
            $table->string('twilio_phone',35)->nullable();
            $table->string('email',50)->unique();
            $table->string('username',50)->unique();
            $table->string('password',70);
            $table->enum('prefered_method', ['phone', 'email','both']);
            $table->enum('status', ['active','inactive'])->default('inactive');
            $table->string('device_token')->nullable();
            $table->string('device_id')->nullable();
            $table->string('device_type')->nullable();
            $table->rememberToken();
            $table->timestamps();
         });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
