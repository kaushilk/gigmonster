<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCardDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_card_details', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('card_type', ['Visa','Mastercard','Discover','AmericanExpress']);
            $table->string('first_name',50);
            $table->string('last_name',50);
            $table->string('card_number',60);
            $table->char('expiry_month',2);
            $table->char('expiry_year',2);
            $table->char('cvv',4);
            $table->enum('primary_method', ['yes','no'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_card_details');
    }
}
