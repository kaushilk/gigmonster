<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('txt_id');
            $table->string('payment_method', 100);
            $table->enum('payer_status', ['pending','completed','rejected'])->nullable();
            $table->enum('payment_state', ['pending','completed','rejected'])->nullable();   
            $table->string('payer_email', 100); 
            $table->double('total', 10, 2);
            $table->double('sub_total', 10, 2);
            $table->double('tax', 10, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
