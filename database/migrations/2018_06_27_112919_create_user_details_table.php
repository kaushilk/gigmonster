<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedInteger('category_id')->nullable();
            $table->foreign('category_id')->references('id')->on('categories')->nullable();
            $table->text('sub_category_id')->comment('sub categories foreign key')->nullable();
            $table->text('address')->nullable();
            $table->string('availability',50)->nullable();
            $table->string('daytime_and_evenings',100)->nullable();
            $table->string('band_name')->nullable();
            $table->text('band_size')->nullable();
            $table->text('band_members')->nullable();
            $table->enum('travel', ['yes','no'])->nullable();
            $table->integer('how_far')->nullable();
            $table->enum('own_gear', ['yes','no'])->nullable();
            $table->enum('jam_session', ['Player','Organizer'])->nullable();
            $table->date('birthdate')->nullable(); 
            $table->enum('gender', ['Male','Female'])->nullable();
            $table->string('experience')->nullable();
            $table->string('external_link1',100)->nullable();
            $table->string('external_link2',100)->nullable();
            $table->text('genres')->nullable();
            $table->text('description')->nullable();
            $table->text('influence')->nullable();
            $table->enum('gigs', ['0to2','3to5','6to8','morethan9','N/A'])->nullable();
            $table->string('profile_pic',100)->nullable();
            $table->enum('sight_read', ['yes','no'])->default('no');
            $table->enum('chart_read', ['yes','no'])->default('no');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_details');
    }
}
