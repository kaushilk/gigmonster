<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentMonthlyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_monthly', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cycle_name');
            $table->char('cycle_month',2);
            $table->double('amount', 10, 2);
            $table->double('billed', 10, 2);
            $table->double('saving', 10, 2)->nullable();
            $table->char('hide',1)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_monthly');
    }
}
