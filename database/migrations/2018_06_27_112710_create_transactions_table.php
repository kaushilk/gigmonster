<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('purpose', 20)->nullable();
            $table->enum('state', ['approved','disapproved'])->nullable();
            $table->string('payment_id')->nullable();
            $table->string('intent',20)->nullable();
            $table->double('amount', 10, 2)->nullable();
            $table->string('currency',20)->nullable();
            $table->string('payment_method',20)->nullable();
            $table->string('credit_card_id',100)->nullable();
            $table->string('txn_id',100)->nullable();
            $table->enum('status', ['pending','completed','rejected'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
