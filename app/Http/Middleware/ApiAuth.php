<?php

namespace App\Http\Middleware;

use Closure;
use App\Helpers\Common\Common;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    
    public function handle($request, Closure $next)
    {

        try{
            $user = Common::checkApisSession($request->header('Authorization'));
            if(empty($user)){
                return response()->json(['message'=>'Token Required','status'=>4],499);
            }
            $request['user'] = $user;
                
        } catch(exception $e){
            if(empty($user)){
                return response()->json(['message'=>'Token Expired','status'=>4],440);
            } else if(empty($request->token)){
                return response()->json(['message'=>'Token Required','status'=>4],499);
            }

        }

        return $next($request);
    }
}
