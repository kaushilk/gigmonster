<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class HomeController extends Controller
{
    public function dashboard()
    {
        $user = Auth::user();
        
        return view('admin.dashboard',compact('user'));
    }
}
