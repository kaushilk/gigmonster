<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Validator;
use Input;
use Redirect;
use Auth;
use App\User;
use App\model\UserActivation;
use App\Events\SendMail;
use DB;

class LoginController extends Controller
{
    private $message;
    public function login(Request $request)
    {
       
        $data = $request->all();

        // dump($data);
        $rules = [
            'email' => 'required|email',
            'password' => 'required',
        ];

        $validator = Validator::make($data,$rules);

        if($validator->fails())
        {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        if(Auth::attempt(['email' => $data['email'], 'password' => $data['password']]))
        {
              //auth success
            $user = auth()->user();   
            return response()->json(['status'=>true]);
            // return Redirect::to('admin/dashboard');
        }
        else
        {
             Auth::logout();
             session()->flash('msg','You Not Authorized Person to allow login at');
             return response()->json(['status'=>false]);
             // return back();
        }
    }

    public function register(Request $request){
        DB::beginTransaction();
        $return['status']=false;
        try{
            $rules=[
                'name'=>'required',
                'email'=>'required|email|unique:users,email',
                'password'=>'required|confirmed',
                'password_confirmation'=>'required'
            ];

            $validator = Validator::make($request->all(), $rules);
            
            if ($validator->fails()) {
                return $return=['msg'=>implode($validator->messages()->all(':message<br>'))];
            }
            $user_data=$request->only('name','email');
            $user_data['password']=Hash::make($request->password);
            $add_user_data=User::create($user_data);
            if($add_user_data){
                $user_token = md5(microtime());
                $user_id=$add_user_data->id;
                $add_user_token=UserActivation::create(['user_id'=>$user_id,'code'=>$user_token]);
                if($add_user_token){
                    $return=['status'=>true,'msg'=>'Registration successfully...'];
                    $user_data=['name'=>$add_user_data->name,'email'=>$add_user_data->email,'token'=>$user_token];
                    event(new SendMail($user_data));
                }
            } 
            
        }
        catch(\Exception $e){
            DB::rollback();
            return ['status'=>false,'msg'=>'Something went wrong.'];
        }
        DB::commit();
        return $return;
    }

    public function logout(){

        Auth::logout();
        return redirect('/');
    }
}
