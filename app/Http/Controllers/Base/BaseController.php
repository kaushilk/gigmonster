<?php

namespace App\Http\Controllers\Base;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\model\UserDetails;
use App\model\Categories;
use App\model\SubCategories;
use App\model\Instruments;
use App\model\UserGeners;
use App\model\UserInstrumentPlayed;
use App\model\UserEquipments;
use App\model\UserBacklines;
use App\model\UserMedia;
use Exception;
use DB;
use File;

class BaseController extends Controller
{
	//Get all cetegories
    protected function getCategory(){
		 $category=Categories::all()->toArray();
		 return ['category'=>$category];
	}

	//Get usergenerallist
	protected function getUserGeneralList($user_prefered_method=null,$user_id=null){
		//Get All instruments list
		$instruments=Instruments::orderBy('id','desc')->get()->toArray();
		//Get user Generes
		$user_geners=UserGeners::orderBy('id','desc')->get()->toArray();
		//Get another user which have same prefered method as login user
		$band_members=User::getBandMembers($user_prefered_method,$user_id);

		return ['instruments'=>$instruments,'user_geners'=>$user_geners,'band_members'=>$band_members];
	}

	// get musician details
	protected function getMusicianDetails($user_id,$category_id){
		try{
			$user=User::with('userDetails','instrumentsPlayed','getUserEquipments','getUserBacklines')
			->where('id',$user_id)
			->first();
			$user_details=$user->userDetails;
			if($user_details){
				$user_details['sub_category_id']=explode(",",$user_details['sub_category_id']);
				$user_details['band_members']=explode(",",$user_details['band_members']);
				$user_details['genres']=explode(",",$user_details['genres']);
				$user_details['profile_pic']=url($user_details['profile_pic']);
				$user_details['influence']=explode(",",$user_details['influence']);
			}
			//Get musician category
			$musician_category=SubCategories::getSubCategoriesByid($category_id);
			//Get user audio
			$user_audio=UserMedia::getUserAudio($user_id);
			//Get user videos
			$user_video=UserMedia::getUserVideo($user_id);
			//Get user images
			$user_images=UserMedia::getUserImages($user_id);
			//User Basic details get in another array
			$user_base_data=collect($user);
			
			$user_basic_details=$user_base_data->except(
				'social_id','twilio_sid','device_token','user_details.id','user_details.user_id'
				);
			
			//Get sub-categories musician
			// $musician_category=SubCategories::getSubCategoriesByid($category_id);
			//Get another user which have same prefered method as login user
			$user_basic_details['audio']=$user_audio;
			$user_basic_details['video']=$user_video;
			$user_basic_details['images']=$user_images;
			$user_basic_details['musician']=$musician_category;
			return $user_basic_details;
		}catch(Exception $e){
			return $this->sendError('Something went wrong.');
		}
	}

	//get musician support details
	protected function getMusicianSupportDetails($user_id,$category_id){
		// $user=User::join('user_details','users.id','=','user_details.user_id')
		// 	->where('users.id',$user_id)
		// 	->select('users.first_name','users.last_name','users.country','users.state','users.city','users.zipcode','users.phone','users.username','user_details.address','user_details.external_link1','user_details.external_link2','user_details.description')
		// 	->first();
		$user=User::with('userDetails')
			->where('id',$user_id)
			->first();
		$user_details=$user->userDetails;
		if($user_details){
			$user_details['sub_category_id']=explode(",",$user_details['sub_category_id']);
			$user_details['profile_pic']=url($user_details['profile_pic']);
		}
		$user_basic_details=collect($user);
		$musician_support_category=SubCategories::getSubCategoriesByid($category_id);
		$user_images=UserMedia::getUserImages($user_id);
		$user_basic_details['musician_support']=$musician_support_category;
		$user_basic_details['images']=$user_images;
		return $user_basic_details;
	}

	protected function updateUserProfile($table,$where=array(),$data=array()){
		try{
			$update=DB::table($table)->where($where)->update($data);
			return $update;
		}catch(Exception $e){
			return $this->sendError('Something went wrong.');
		}
	}
	//Add user images
	public function uploadFile($prefix,$image,$path,$data=0){
    	$image_name=$prefix.'_'.uniqid().$data.'.'.$image->getClientOriginalExtension();
    	$upload_image=$image->move(base_path($path), $image_name);
    	return $path."/".$image_name;
    }

    //Update User Instruments
    public function updateUserInstruments($user_id,$instruments){
    	try{

    		$user = UserInstrumentPlayed::where('user_id',$user_id);
    		if($user->exists()){
    			$user->delete();
    		}
			foreach ($instruments as $value) {
				$skill = $value->skill_level == '' ? null : $value->skill_level;
				$insert_instruments=UserInstrumentPlayed::create(['user_id'=>$user_id,'instrument_id'=>$value->instrument_id,'skill_level'=>$skill]);
			}
    	}catch(Exception $e){
    		return $this->sendError('Something went wrong.');	
    	}
    	
    }
    //Update user Equipments
    public function updateUserEquipments($user_id,$equipments){
    	try{
			$user_equipment = UserEquipments::where('user_id',$user_id);
			if($user_equipment->exists()){
    			$user_equipment->delete();
    		}	
			foreach ($equipments as $value) {
				$insert_equipments=UserEquipments::create([
					'user_id'=>$user_id,'equipment_name'=>$value->equipment_name,'byog'=>$value->byog
				]);
			}
    	}catch(Exception $e){
    		return $this->sendError('Something went wrong.');
    	}
    }
    //Update user backlines
    public function updateUserBackilnes($user_id,$backlines){
    	try{
    		//Check user backlines availble
    		$user_backlines = UserBacklines::where('user_id',$user_id);

			if($user_backlines->exists()){
				$user_backlines->delete();
			}
			foreach ($backlines as $value) {
					$insert_backlines=UserBacklines::create([
						'name'=>$value->name,'user_id' => $user_id
					]);
			}
    	}catch(Exception $e){
    		return $this->sendError('Something went wrong.');	
    	}
    }
    //Add or update user audio
    public function updateUserAudio($user_id,$music_file){
    	$audio=UserMedia::where(['user_id'=>$user_id,'type'=>'music'])->select('music_file')->first();
    	if(!empty($audio->music_file) && File::exists($audio->music_file)){
    		File::delete($audio_file);
    	}
    	$user_media['music_file']=$this->uploadFile('audio',$music_file,'upload/audios');
    	$user_media['type']='music';
    	$update_user_audio=UserMedia::updateOrCreate(['user_id'=>$user_id,'type'=>'music'],$user_media);
    	return $update_user_audio;
    }

    //Add or update user video
    public function updateUserVideo($user_id,$video_url){
    	try{
    		$video['video_url']=$video_url;
    		$video['type']='video';
    		$update_user_video=UserMedia::updateOrCreate(['user_id'=>$user_id,'type'=>'video'],$video);
    		return $update_user_video;
    	}catch(Exception $e){
    		return $this->sendError('Something went wrong.');	
    	}
    }

    public function updateUserImage($user_id,$image_file,$image_id){
    	try{
    		$user_image_exist=UserMedia::where(['user_id'=>$user_id,'type'=>'image']);
    		if($user_image_exist->exists()){
    		//Update user image
	    		$user_images=$user_image_exist->pluck('image_file');
	    		$i=0;
	    		$count_id=0;
	    		foreach($image_file as $value) {
	    			$image['image_file']=$this->uploadFile('image',$value,'upload/images',$i);
	    			$image['type']='image';
	    			$image['user_id']=$user_id;
	    			if(!empty($image_id[$i])){
	    				$update_image=UserMedia::where(['user_id'=>$user_id,'type'=>'image','id'=>$image_id[$i]])->update($image);
	    			}else{
	    				$insert_image=UserMedia::create($image);
	    			}
				$i++;
	    		}
	    	}else{
	    		$i=0;
	    		foreach($image_file as $value) {
	    			$image['user_id']=$user_id;
	    			$image['image_file']=$this->uploadFile('image',$value,'upload/images',$i);
	    			$image['type']='image';
	    			$add_image=UserMedia::create($image);
	    			$i++;
	    		}
	    	}
    	}catch(Exception $e){
    		// dd($e);
    		return $this->sendError('Something went wrong.');
    	}
    }
    //For get epk file
    public function generateEpkFile(){

    }

    //For Faviourite gig group
    public function favouriteGigGroup($user_id,$city){
    	try{
    		$member_id=User::join('user_gig_groups','users.id','=','user_gig_groups.user_id')
		    	->where(['users.id'=>$user_id,'users.city'=>$city])
		    	->select('user_gig_groups.member_id')
		    	->pluck('user_gig_groups.member_id');
	    	
	    	$user_details=UserDetails::whereIn('user_id',$member_id)->select('user_id','profile_pic')->limit(8)->get();
	    	$member_data=[];
	    	foreach ($user_details as $value) {
	    		$member_data[]=['id'=>$value->user_id,'profile_pic'=>$value->profile_pic];
	    	}
	    	return $member_data;
    	}catch(Exception $e){
    		return false;
    	}
    }
    //For Local gig group
    public function localGigGroup($user_id,$city){
    	try{
    		$member_id=User::join('user_gig_groups','users.id','=','user_gig_groups.user_id')
		    	->where('users.id',$user_id)
		    	->where('users.city','!=',$city)
		    	->select('user_gig_groups.member_id')
		    	->pluck('user_gig_groups.member_id');
	    	
	    	$user_details=UserDetails::whereIn('user_id',$member_id)->select('user_id','profile_pic')->limit(8)->get();
	    	$member_data=[];
	    	foreach ($user_details as $value) {
	    		$member_data[]=['id'=>$value->user_id,'profile_pic'=>$value->profile_pic];
	    	}
	    	return $member_data;
    	}catch(Exception $e){
    		return false;
    	}
    }
}
