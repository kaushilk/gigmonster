<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\api\APIBaseController as APIBaseController;
use Exception;
use App\User;
use App\model\UserDetails;
use App\model\Categories;
use App\model\SubCategories;
use App\model\Instruments;
use App\model\UserGeners;
use App\model\UserInstrumentPlayed;
use App\model\UserEquipments;
use App\model\UserBacklines;
use App\model\UserMedia;
use Validator;
use File;

class UserController extends APIBaseController
{
	
	//Edit user profile
	public function editProfile(Request $request){
		try{
			$user_basic_details=[];
			$user_id=$request->user->id;
			//Get member type which are available in category
			//$member type =category
			$member_type=$request->user->member_type;
			$prefered_method=$request->user->prefered_method;
			//Get category id for find subcategories
			// $category_id=Categories::where('name',$member_type)->value('id');
			// dd($member_type);
			// dd($category_id);

			switch ($member_type) {
				case '2':
					$musician_details=collect($this->getMusicianDetails($user_id,$member_type))->toArray();
					// dd($musician_details);
					$user_general_details=$this->getUserGeneralList($prefered_method,$user_id);
					//Combine user detsils and all dynamic fields
					$user_data=array_merge($musician_details,$user_general_details);
					return $this->sendResponse('Edit Successfully',$user_data);
					break;
				case '1':
					$musician_support_details=$this->getMusicianSupportDetails($user_id,$member_type);
					return $this->sendResponse('Edit Successfully',$musician_support_details);
					break;
				default:
					//Get user registration details
					/*$user_basic_details=collect(User::where('id',$user_id)
					->select('id','first_name','last_name','country','state','city','zipcode','phone','email','username')
					->first())->toArray();
					
					$request_category_id=$request->get('category_id',2);
					//Get category name
					$category_name=Categories::where('id',$request_category_id)->value('name');
					//Get subcategories by parent category
					$sub_category[$category_name]=SubCategories::getSubCategoriesByid($request_category_id);

					$category=$this->getCategory();

					$user_general=$category_name=="Musician"?$this->getUserGeneralList():[];
					// $user_data[$request_category_name];
					return $this->sendResponse('Edit Successfully',array_merge($user_basic_details,$category,$sub_category,$user_general));*/
					return $this->sendError('Something went wrong.');
					break;
			}

		}catch(Exception $e){
			return $this->sendError('Something went wrong.');
		}
	}

	//Update user profile
	public function updateProfile(Request $request){
		try{
			// var_dump(request('sub_category_id'));
			// dd(request('sub_category_id'));
			// $string=substr(request('sub_category_id'), 1, -1);
			// dd($string);
			if(!empty($request->screen) && $request->screen=="1"){
				$rule=[
	                'first_name'=>'required|alpha|min:3|max:18',
	                'last_name'=>'required|alpha|min:3|max:18',
	                'country'=>'required',
	                'state'=>'required|alpha|min:2',
	                'city'=>'required',
	                'zipcode'=>'required',
	            ]; 
	            
	            $validate = Validator::make($request->all(),$rule);
	            if ($validate->fails())
	            {   
	                return $this->sendError('Validation Error.', $validate->errors());     
	            }
			}
			
			// Get user id
			$user_id=$request->user->id;
			$user=$request->only('first_name','last_name','country','state','city','zipcode');
			//Update user 
			$update_user=User::where('id',$user_id)->update($user);

			$user_details=$request->only('birthdate','gender','sub_category_id','gigs','availability',
										'daytime_and_evenings','address','band_name','band_size','band_members',
										'travel','how_far','own_gear','jam_session','experience','external_link1',
										'external_link2','genres','description','influence','profile_pic',
										'sight_read','chart_read');
			//Upload Profile pic
			if($request->hasFile('profile_pic')){
				$image=UserDetails::where('user_id',$user_id)->select('profile_pic')->first();
				// dd($image->profile_pic);
				if(!empty($image->profile_pic) && File::exists($image->profile_pic)){
					 File::delete($image->profile_pic);
				}
				$user_details['profile_pic']=$this->uploadFile('profile',$request->profile_pic,'upload/profile');
			}
			
			$update_user_details=UserDetails::updateOrCreate(['user_id'=>$user_id],$user_details);

			//For User Instruments Played

			if($request->filled('user_instruments')){
				$instruments=$request->user_instruments;
				// dd($user_id);

				$update_user_instruments=$this->updateUserInstruments($user_id,json_decode($instruments));
			}
			
			//User Equipments
			if($request->filled('user_equipments')){
				$euipments=$request->user_equipments;
				$update_user_equipments=$this->updateUserEquipments($user_id,json_decode($euipments));
			}

			//User Backlines
			if($request->filled('user_backlines')){
				$backlines=$request->user_backlines;
				$update_user_backline=$this->updateUserBackilnes($user_id,json_decode($backlines));
			}

			
			//Add or update user audio
			if($request->hasFile('audio')){
				$audio_file=$request->audio;
				$update_user_audio=$this->updateUserAudio($user_id,$audio_file);
			}

			//Add or update user video
			if($request->has('video')){
				$video_url=$request->video;
				$update_user_video=$this->updateUserVideo($user_id,$video_url);
			}

			if($request->has('image')){
				$images=request('image');
				$image_id=json_decode(request('image_id'));
			 	$update_user_image=$this->updateUserImage($user_id,$images,$image_id);
			}

			// dd($update_user_details);
			return $this->sendResponse('User Updated.');
		}catch(Exception $e){
			// dd($e);
			return $this->sendError('Something went wrong.');
		}
	}

	//View User Profile
	public function viewProfile(Request $request){
		try{
			$user_id=$request->user->id;
			$city=$request->user->city;
			$member_type=$request->user->member_type;
			$favourite_gig=$this->favouriteGigGroup($user_id,$city);
			$local_gig=$this->localGigGroup($user_id,$city);
			$prefered_method=$request->user->prefered_method;
			switch($member_type){
				case '2':
						$musician_details=collect($this->getMusicianDetails($user_id,$member_type))->toArray();
						//Get user general details
						$user_general_details=$this->getUserGeneralList($prefered_method,$user_id);
						//Add general list
						$musician_details['instruments']=$user_general_details['instruments'];
						$musician_details['user_geners']=$user_general_details['user_geners'];
						$musician_details['band_members']=$user_general_details['band_members'];
						$musician_details['favourite_gig_group']=$favourite_gig;
						$musician_details['local_gig_group']=$local_gig;
						return $this->sendResponse('View Successfully',$musician_details);
						break;
				case '1':
					$musician_support_details=$this->getMusicianSupportDetails($user_id,$member_type);
					$musician_support_details['favourite_gig']=$favourite_gig;
					$musician_support_details['local_gig']=$local_gig;
					return $this->sendResponse('View Successfully',$musician_support_details);
					break;
				default:
					return $this->sendError('Something went wrong.');
					break;
			}
		}catch(Exception $e){
			// dd($e);
			return $this->sendError('Something went wrong.');
		}
	}
}
