<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use App\Http\Controllers\Base\BaseController;


class APIBaseController extends BaseController
{
     public function sendResponse($message,$result=null)
    {
    	$response = [
            'status' => 1,
            'data'    => $result,
            'message' => $message,
        ];


        return response()->json($response, 200);
    }

     public function sendError($error, $errorMessages = [], $code = 406)
    {
    	$response = [
            'status' => 0,
            'message' => $error,
        ];


        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }


        return response()->json($response, $code);
    }
    

}
