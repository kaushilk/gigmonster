<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use Response;
use App\Http\Controllers\Controller;
use App\Http\Controllers\api\APIBaseController as APIBaseController;
use Validator;
use Auth;
use Role;
use DB;
use Hash;
use App\User;
use URL;
use App\Helpers\Common\Common;
use App\model\ApiSession;
use Illuminate\Support\Facades\Route;
use Exception;
use Token;
use App\model\UserActivation;
use App\Events\SendMail;
use Carbon\Carbon;
use Twilio;



class AuthController extends APIBaseController
{
    //Usre api registration
    public function register(Request $request){
        DB::beginTransaction();
        try{
            $rule=[
                'first_name'=>'required|alpha|min:3|max:18',
                'last_name'=>'required|alpha|min:3|max:18',
                'country'=>'required',
                'state'=>'required|alpha|min:2',
                'city'=>'required',
                'zipcode'=>'required',
                'phone'=>'required|unique:users,phone|regex:/^(\+\d{1,2}[-]?)?\d{10}$/u',
                'email'=>'required|unique:users,email|email',
                'username'=>'required|unique:users,username|min:3|max:18|alpha_num',
                'password'=>'required_without:social_id|between:6,12',
                'member_type'=>'required',
                'prefered_method'=>'required|in:phone,email,both',
                'status'=>'required_with:social_id'
            ]; 
            $message=[
                'phone.regex'=>'Mobile number must be 10 digits.',
                'prefered_method.in'=>'Prefered method must be phone,email or both'
            ];
            $validate = Validator::make($request->all(),$rule,$message);
            if ($validate->fails())
            {   
                return $this->sendError('Validation Error.', $validate->errors());     
            }
            $user_data=$request->all();
            $user_data['password']=Hash::make($request->password);
            // dd($request->all());
            $user=User::create($user_data);
            if($user){
                //Registration with social id
                if($request->filled('social_id')){
                    //Add token response to user
                    $api_token = Common::setSessionData($user->id);
                    DB::commit();
                    return $this->sendResponse('Login successfully.',['token'=>$api_token,'user'=>$user]);
                }
                // $token=Token::UniqueNumber('user_activation','code',6);
                //Give 6 digits token
                $token=Common::getToken();
                $add_user_token=UserActivation::create(['user_id'=>$user->id,'code'=>$token]);
                if($add_user_token){
                    $mail_data=['name'=>$user_data['first_name'].' '.$user_data['last_name'],'code'=>$token];
                    // event(new SendMail($mail_data));
                    \Mail::to($user->email)->send(new \App\Mail\RegisterMail($mail_data));
                    try{
                        Twilio::message($request->phone,$token);
                    }catch(Exception $e){
                        return $this->sendError('Mobile number  is not valid.');
                    }
                    DB::commit();
                    return $this->sendResponse('User registered successfully.Please check your mail',['user'=>$user]);
                }
            }
            return $this->sendError('Unable to add user.');
        }catch(Exception $e){
            // dd($e);
            DB::rollback();
            return $this->sendError('Something went wrong.');
        }
    }
    //User login through api
    public function login(Request $request){
        try{
            //For a facebook login 
            if($request->login_type=="facebook"){
                $social_id=$request->social_id;
                $user_details=User::where(['social_id'=>$social_id,'status'=>'active'])->first();
                if($user_details){
                    //Generate token base on user id
                    $token = Common::setSessionData($user_details->id);
                    $data = array('token' => $token, 'user' => $user_details);
                    return $this->sendResponse('Login successfully.',$data);
                }
                //User not registered with facebook
                return ['status'=>3,'message'=>'Facebook account not found in system.'];
            }
            $rule=[
                'email' => 'required',
                'password' => 'required',
            ];
            $validate = Validator::make($request->all(), $rule);
            if($validate->fails())
            {
                return $this->sendError('Validation Error.', $validate->errors());     
            }
            $email=$request->email;
            $user_data = User::where('email', $email)->orWhere('phone',$email)->first();
            if($user_data)
            {
                if(Hash::check($request->get('password'), $user_data->password)) 
                {
                    $user=array('user'=>$user_data);
                    if($user_data->status=='inactive'){
                        return ['status'=>2,'message'=>'Account verification pending.','data'=>$user];
                    }
                    $token = Common::setSessionData($user_data->id);
                    $data = array('token' => $token, 'user' => $user_data);
                    return $this->sendResponse('Login successfully.',$data);
                }
                else
                {
                    return $this->sendError('Invalid Password..!.');
                }
            }
            else 
            {
                return $this->sendError('Invalid email..!.');
            }

        }catch(Exception $e){
            // dd($e);
            return $this->sendError('Something went wrong.');
        }
    }
    //Verify user
    public function verify(Request $request){
        try{
            $rule=[
                'otp' => 'required',
            ];
            $validate = Validator::make($request->all(), $rule);
            if($validate->fails())
            {
                return $this->sendError('Validation Error.', $validate->errors());     
            }
            $otp=$request->get('otp');
            $user_details=UserActivation::where('code',$otp)
            ->whereRaw('created_at >= NOW() - INTERVAL 10 MINUTE')
            ->select('user_id','code')
            ->first();
            if($user_details){
                $user_id=$user_details->user_id;
                $user_status_update=User::where(['id'=>$user_id,'status'=>'inactive'])->update(['status'=>'active']);
                $user_data=User::find($user_id);
                if($user_status_update){
                    //Generate api token base on user id
                    $api_token = Common::setSessionData($user_id);
                    //Get user details from token
                    $data = array('token' => $api_token, 'user' => $user_data);
                    //Delete user token after verification.
                    $delete_token=UserActivation::where(['code'=>$otp])->delete();
                    return $this->sendResponse('Verification successfully',$data);
                }
            }
            //If token expire send token expire response
            return ['status'=>4,'message'=>'Token is expired.'];
        }catch(Exception $e){
            // dd($e->getMessage());
            return $this->sendError('Something went wrong.');
        }
    }

    //Forgot Password
    public function forgotPassword(Request $request){
        try{
            $rule=[
                'email' => 'required_without:phone',
                'phone'=>'required_without:email'
            ];
            $validate = Validator::make($request->all(), $rule);
            if($validate->fails())
            {
                return $this->sendError('Validation Error.', $validate->errors());     
            }
            $email=$request->email;
            $phone=$request->phone;
            $user_details=User::where('email',$email)->orWhere('phone',$phone)->first();
            ///User registered then send verification token
            if($user_details){
                ///User not verify then send account verification message.
                if($user_details->status=="inactive"){
                    return ['status'=>2,'message'=>'Account verification pending.'];
                }
                //Give 6 digits token
                $token=Common::getToken();
                $add_user_token=UserActivation::create(['user_id'=>$user_details->id,'code'=>$token]);
                if(!empty($email)){
                    //Send mail
                    $user_name=$user_details->first_name.' '.$user_details->last_name;
                    $user_data=['name'=>$user_name,'code'=>$token];
                    \Mail::to($email)->send(new \App\Mail\ForgotPassword($user_data));
                    return $this->sendResponse('Verification mail send successfully.Please check your mail.');
                }elseif (!empty($phone)) {
                    //Send messaage user phone
                    try{
                        Twilio::message($phone,$token);
                        return $this->sendResponse('Verification message send successfully.Please check your message.');
                    }catch(Exception $e){
                        return $this->sendError('Mobile number  is not valid.');
                    }
                }
            }
           return $this->sendError('Entered email/phone could not be found.');
        }catch(Exception $e){
            // dd($e->getMessage());
            return $this->sendError('Something went wrong.');
        }
    }
    //Reset Password after verify
    public function resetPassword(Request $request){
        try{
            $rule=[
                'code' => 'required',
                'password' => 'required|same:confirm_password',
                'confirm_password'=>'required:same:password'
            ];
            $validate = Validator::make($request->all(), $rule);
            if($validate->fails())
            {
                return $this->sendError('Validation Error.', $validate->errors());     
            }
            $code=$request->code;
            $user_code=UserActivation::where('code',$code)->select('code','user_id')->first();
            if($user_code){
                //Get userid base on a code
                $userid=$user_code->user_id;
                $password=$request->password;

                //Update a user password 
                $update_password=User::where(['id'=>$userid,'status'=>'active'])->update(['password'=>Hash::make($password)]);
                if($update_password){
                    //Delete user activation code after successfully update password
                    $delete_code=UserActivation::where('code',$code)->delete();
                    return $this->sendResponse('Password is updated,Please try to Login.');
                } 
            }
            return $this->sendError('Please enter valid verification code.');
            
        }catch(Exception $e){
            return $this->sendError('Something went wrong.');
        }
    }
    //Resend verification link
    public function resendLink(Request $request){
        DB::beginTransaction();
        try{
            if($request->filled('user_id')){
                $user_id=$request->user_id;
                $token=Common::getToken();
                $add_user_token=UserActivation::create(['user_id'=>$user_id,'code'=>$token]);
                if($add_user_token){
                   //Get user details
                    $user_data=User::where('id',$user_id)->select('first_name','last_name','email','phone','id')->first(); 
                    $mail_data=['name'=>$user_data['first_name'].' '.$user_data['last_name'],'code'=>$token];
                    //Check the link type and send mail base on it
                    $email=$user_data->email;
                    $phone=$user_data->phone;
                    try{
                        \Mail::to($email)->send(new \App\Mail\RegisterMail($mail_data));
                        Twilio::message($phone,$token);
                    }catch(Exception $e){
                        return $this->sendError('Unable to send verification link.');
                    }
                    DB::commit();
                    return $this->sendResponse('New activation code send successfully.');
                }
            }
            return $this->sendError('Unable to send link or user not found.');
        }catch(Exception $e){
            DB::rollback();
            // dd($e->getMessage());
            return $this->sendError('Something went wrong.');
        }
    }
    //Logout user
    public function logout(Request $request){
        try{
            //Get token and null that token from a api
            $session_token=$request->token;
            $user_id=$request->user_id;
            $user_logout=ApiSession::where(['session_id'=>$session_token,'user_id'=>$user_id])->update(['session_id'=>NULL]);
            return $this->sendResponse('User logout successfully.');
        }catch(Exception $e){
            return $this->sendError('Something went wrong.');
        }
    }
}
