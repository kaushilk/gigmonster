<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class UserActivation extends Model
{
	protected $table = 'user_activation'; 
	protected $fillable = ['user_id','code','status'];
}
