<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class UserDetails extends Model
{
    
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    protected $fillable=[
    	'birthdate','gender','sub_category_id','gigs','availability',
		'daytime_and_evenings','address','band_name','band_size','band_members',
		'travel','how_far','own_gear','jam_session','experience','external_link1',
		'external_link2','genres','description','influence','profile_pic',
		'sight_read','chart_read','user_id','category_id'
    ];

    //Get user details by id
	public function user()
	{
	    return $this->belongsTo('App\User', 'user_id');
	}

	public static function getFavouriteGigGroup(){
        return static::join('user_gig_groups','user_gig_groups.user_id','=','users.id')
        
        // ->where('user_gig_groups.member_id','users.id')
        ->get()->pluck('member_id');
    }
}
