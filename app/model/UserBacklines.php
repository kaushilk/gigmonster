<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class UserBacklines extends Model
{
	protected $hidden=['created_at','updated_at'];

	protected $fillable=['user_id','backline_id','name'];
	//Get user backlines
    public function UserBacklines()
	{
	    return $this->belongsTo('App\User', 'user_id');
	}
}
