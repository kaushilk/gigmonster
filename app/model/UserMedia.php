<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use DB;

class UserMedia extends Model
{
	protected $hidden=['created_at','updated_at'];

    protected $guarded=['id'];

	//Get User Images,Photos and Videos
    public function userMedia()
	{
	    return $this->belongsTo('App\User', 'user_id');
	}
	//Get user audio
	public static function getUserAudio($user_id=null)
    {
         $audio=static::where(['type'=>'music','user_id'=>$user_id])
        	->select('music_title','music_year','music_album','music_genre','tracknumber','music_file')
        	->first();
        if($audio){
        	$audio['music_file']=file_exists($audio['music_file'])?url($audio['music_file']):url(config('constant.default_image_url'));
            return $audio;
        }
        
    }
    //Get user video
    public static function getUserVideo($user_id=null)
    {
        return static::where(['type'=>'video','user_id'=>$user_id])
        	->select('video_tilte','video_url')
        	->get();
    }
    //Get user images
    public static function getUserImages($user_id=null)
    {
        $images= static::where(['type'=>'image','user_id'=>$user_id])
        	->select(DB::raw('id as image_id'),'image_title','image_description','image_file')
        	->get();
        if($images){
        	foreach ($images as $value) {
        		$value->image_file=file_exists($value->image_file)?url($value->image_file):url(config('constant.default_image_url'));
	        }
			return $images;
        }
        
    }
}
