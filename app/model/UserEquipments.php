<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class UserEquipments extends Model
{
    protected $hidden=['created_at','updated_at'];

    protected $fillable=['user_id','equipment_name','byog'];

    //Get user equipments
    public static function getUserEquipments($id=null){
        return static::where('user_id',$id)->get();
    }
}
