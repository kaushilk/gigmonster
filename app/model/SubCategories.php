<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class SubCategories extends Model
{
    //Give subcategories name with id
    public static function getSubCategoriesByid($id=null){
    	return static::where('category_id',$id)
                ->select('id','subcategory_name','category_id')
                ->get()->toArray();
    }
    //Get category
    public function getSubCategories()
	{
	    return $this->belongsTo('App\modal\Categories', 'category_id');
	}


}
