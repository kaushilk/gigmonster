<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class UserInstrumentPlayed extends Model
{
	protected $table='user_instrument_played';
	protected $hidden=['created_at','updated_at'];

	protected $fillable=['user_id','instrument_id','skill_level'];
    //Get instrument pladed by user
    public function userInstruments()
	{
	    return $this->belongsTo('App\User', 'user_id');
	}
}
