<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Instruments extends Model
{
    protected $fillable=['id','name','sub_instrument_id'];
}
