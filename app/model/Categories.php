<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
	protected $hidden=['created_at','updated_at'];
	//Get sub-categories
    public function getSubCategories()
    {
        return $this->hasMany('App\model\SubCategories','category_id');
    }
}
