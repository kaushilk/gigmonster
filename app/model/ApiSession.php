<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use App\User;

class ApiSession extends Model
{
   	protected $table = 'api_session';

	protected $fillable = [
		'session_id', 'user_id', 'login_time' 
		];

	public function user_data()
	{
	 	return $this->belongsTo('App\User','user_id');
	}
	//Return user info base on token
	public static function GetUser($token){
		$user_id=static::where('session_id',$token)->value('user_id');
		$user=User::find($user_id);
		return $user;
    }

    
}
