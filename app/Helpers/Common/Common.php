<?php 

namespace App\Helpers\Common;

use App\model\ApiSession;
use App\User;
use App\model\LogResponse;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Log;

class Common 
{

	public static function setSessionData($userId)
	{
	    if (empty($userId)) {
	        return "User id is empty.";
	    } else {
			/*  FIND USER ID IN API SESSION AVAILABE OR NOT  */
		        // $getApiSessionData = ApiSession::where('user_id',$userId)->first();
		        $getApiSessionData = ApiSession::updateOrCreate(
			    ['user_id' => $userId],
			    ['session_id' =>  md5(rand()),'user_id'=>$userId,'login_time'=>\Carbon\Carbon::now(),'active'=>1]
			);

		    if ($getApiSessionData) {
		    	return $getApiSessionData->session_id;
		    }
		    return false;
		    //Old code
		    
	        // if ($getApiSessionData) {
	        //     if ($getApiSessionData->delete()) {
	        //         $apiSession = new ApiSession();
	        //         /*  SET SESSION DATA  */
	        //         $sessionData = [];
	        //         $sessionData['session_id'] = md5(rand());
	        //         $sessionData['user_id'] = $userId;
	        //         $sessionData['login_time'] = \Carbon\Carbon::now();
	        //         $sessionData['active'] = 1;
	        //         $apiSession->fill($sessionData);
	        //         if ($apiSession->save()) {
	        //             return $apiSession->session_id;
	        //         } else {
	        //             return FALSE;
	        //         }
	        //     } else {
	        //         return FALSE;
	        //     }
	        // } else {
	        //     $apiSession = new ApiSession();
	        //     /*  SET SESSION DATA  */
	        //     $sessionData = [];
	        //     $sessionData['session_id'] = md5(rand());
	        //     $sessionData['user_id'] = $userId;
	        //     $sessionData['login_time'] = \Carbon\Carbon::now();
	        //     $sessionData['active'] = 1;
	        //     $apiSession->fill($sessionData);
	        //     if ($apiSession->save()) {
	        //         return $apiSession->session_id;
	        //         } else {
	        //         return FALSE;
	        //     }
	        // }
	    }
	}

	public static function checkApisSession($sessionId)
	{
		if(!empty($sessionId)){
			$checkSessionExist = ApiSession::with('user_data')->where('session_id', $sessionId)->first();
			if ($checkSessionExist) {
		        // $sessionData = [];
		        // $sessionData['id'] = ($checkSessionExist->id) ? $checkSessionExist->id : '';
		        // $sessionData['session_id'] = ($checkSessionExist->session_id) ? $checkSessionExist->session_id : '';
		        // $sessionData['member_type'] = ($checkSessionExist->user_data['member_type']) ? $checkSessionExist->user_data['member_type'] : '';
		        // $sessionData['prefered_method'] = ($checkSessionExist->user_data['prefered_method']) ? $checkSessionExist->user_data['prefered_method'] : '';
		        // $sessionData['id'] = ($checkSessionExist->user_id) ? $checkSessionExist->user_id : '';
		        // $sessionData['login_time'] = ($checkSessionExist->login_time) ? $checkSessionExist->login_time : '';
		       $checkSessionExist->user_data;
		        return $checkSessionExist->user_data;
		    } 
		}
	    else {
	        return array();
	    }
	}

	public static function responsedata($routeName, $method, $request, $response, $env, $projectName)
	{
		 Log::info($routeName);
		 Log::info($method);
		 Log::info($request);
		 Log::info($response);
		 Log::info($env);
		 Log::info($projectName);

		$checkcondition = config('common.log');

		 if ($checkcondition == 1) 
		 {
		 	$newData = new LogResponse;
		 	$logData['route_name'] = $routeName;
		 	$logData['method'] = $method;
		 	$logData['request'] = json_encode($request);
		 	$logData['response'] = $response;
		 	$logData['environment'] = $env;
		 	$logData['project_name'] = $projectName;

		 	$newData->fill($logData);
	        $newData->save();

	     } 
	}
	//Generate a token for a user verification
	public static function getToken(){
		return mt_rand(100000, 999999);
	}
}

	