<?php 
namespace App\Helpers\Common;

class LibraryAuth{

	public function Register($request){
		$rule=[
            'first_name'=>'required',
            'last_name'=>'required',
            'country'=>'required',
            'state'=>'required',
            'city'=>'required',
            'zipcode'=>'required',
            'phone'=>'required',
            'email'=>'required',
            'username'=>'required',
            'password'=>'required',
            'prefered_method'=>'required',
        ]; 
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails())
        {   
            return $this->sendError('Validation Error.', $validate->errors());     
        }
	}
}
?>