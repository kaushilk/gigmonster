<?php

namespace App\Listeners;

use App\Events\SendMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use Mail;
use Config;

class SendMailFired
{
    public $user;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user=$user;
    }

    /**
     * Handle the event.
     *
     * @param  SendMail  $event
     * @return void
     */
    public function handle(SendMail $event)
    {
        // $user = User::find($event->userId)->toArray();
        $user=$event->user_data;
        Mail::send('admin.email.user_register', $user, function($message) use ($user) {
            $message->from(Config::get('constant.mail_from_email'),Config::get('constant.mail_from_name'));
            $message->to($user['email']);
            $message->subject('Register');
        });
    }
}
