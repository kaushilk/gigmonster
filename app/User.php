<?php
 
namespace App;

use Eloquent;
use Illuminate\Notifications\Notifiable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use EntrustUserTrait;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'social_id','first_name', 'last_name','country','state','city','zipcode','phone','twilio_sid','twilio_phone','email','username','password','prefered_method','status','device_token','device_id','device_type','remember_token','member_type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //Get User Detail
    public function userDetails()
    {
        return $this->hasOne('App\model\UserDetails','user_id');
    }

    //Get user instruments played
    public function instrumentsPlayed()
    {
        return $this->hasMany('App\model\UserInstrumentPlayed','user_id');
    }

    //Get user equipments
    public function getUserEquipments()
    {
        return $this->hasMany('App\model\UserEquipments','user_id');
    }

    //Get user backlines
    public function getUserBacklines()
    {
        return $this->hasMany('App\model\UserBacklines','user_id');
    }

    

    //Favourite gig group

    //Get user audio
    /*public function getUserAudio()
    {
        return $this->hasOne('App\model\UserMedia','user_id')->where('type','music');
    }*/
    //Get user video
    /*public function getUserVideo()
    {
        return $this->hasOne('App\model\UserMedia','user_id')->where('type','video');
    }*/
    //Get user images
    /*public function getUserImages()
    {
        return $this->hasMany('App\model\UserMedia','user_id')->where('type','image');
    }*/
    
    //Get some user details by id
    public static function getUser($id=null){
        return static::where('id',$id)
                ->select('first_name','last_name','country','state','city','zipcode','phone','email','username','prefered_method')
                ->first();
    }
    //Get all another user which have same prefered method as login user
    public static function getBandMembers($prefered_method=null,$user_id=null){
        return static::where('prefered_method',$prefered_method)
                    ->where('id','!=',$user_id)
                    ->select('id','first_name')
                    ->get()->toArray();
    }

    
}
